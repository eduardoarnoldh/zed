import numpy as np
cimport numpy as np
from libcpp.string cimport string
from libc.string cimport memcpy


cdef class ZEDCamera:
    """ZED Camera

    Camera constructor. The ZEDResolution_mode sets the sensor resolution and defines the size of the
    output images, including the measures (disparity map, confidence map..). Alternatively, can be
    used to process a previously recorded SVO file. Either ``SVO_filepath`` or ``resolution_mode``
    must be given to the constructor.

    All computation is done on a CUDA capable device (That means that every CPU computation will need
    a memory retrieve of the images, which takes some time). If the performance is the main focus, all
    external computation should run on GPU. The retrieve*_gpu gives directly access to the gpu buffer.

    Args:
        resolution_mode (optional[zed.ZEDResolution_mode]): the chosen ZED resolution
        SVO_filepath (optional[str]): SVO filepath previously recorded.
        fps (optional[float]): a requested fps for this resolution. set as 0.0 will choose the default
            fps for this resolution ( see User guide)
        zed_linux_id (optional[int]): ONLY for LINUX : if multiple ZEDs are connected, it will choose the
            first zed listed (if zed_linux_id=0), the second listed (if zed_linux_id=1), ... Each ZED
            will create its own memory (CPU and GPU), therefore the number of ZED available will depend
            on the configuration of your computer. Currently not available for Windows
    """

    cdef Camera *thisptr      # hold a C++ instance which we're wrapping

    def __cinit__(self, int resolution_mode=-1, str SVO_filepath=None, float fps=0.0, int zed_linux_id=0):

        if resolution_mode != -1:
            self.thisptr = new Camera(<ZEDResolution_mode>resolution_mode, fps, zed_linux_id)
        elif SVO_filepath is not None:
            self.thisptr = new Camera(<string>SVO_filepath)
        else:
            raise Exception('Either SVO_filepath or resolution_mode should be given')

        self.thisptr.init(PERFORMANCE, -1, True, False)

    def __dealloc__(self):
        del self.thisptr

    property depth_clamp_value:
        def __get__(self):
            return self.thisptr.getDepthClampValue()
        def __set__(self, max_depth):
            self.thisptr.setDepthClampValue(max_depth)

    property image_size:
        def __get__(self):

            cdef int width = self.thisptr.getImageSize().width
            cdef int height = self.thisptr.getImageSize().height

            return width, height

    def grab(self, int mode=RAW, compute_measure=True, compute_disparity=True):
        """Grab a Frame

        The function grabs a new image, rectifies it and computes the disparity map and optionally the depth map.
        The grabbing function is typically called in the main loop.

        Args:
            mode (optional[zed.SENSING_MODE]) : defines the type of disparity map (RAW, FULL).
            compute_measure (optional[Bool]): defines if the depth map should be computed. If false, only the disparity map is computed.
            compute_disparity (optional[Bool]): defines if the disparity map should be computed. If false, the depth map can't be
                computed and only the camera images can be retrieved.

        Returns:
            error : the function returns False if no problem was encountered, True otherwise.
        """

        return self.thisptr.grab(<SENSING_MODE>mode, compute_measure, compute_disparity)

    def normalizeDepth(self, float min=0, float max=0):
        """Retrieve Normalized Depth

        Performs a GPU normalization of the depth value and download the result as a CPU image.

        Args:
            min (optional[float]): defines the lower bound of the normalization, default : automatically found
            max (optional[float]): defines the upper bound of the normalization, default : automatically found

        Returns:
            normalized_depth (array): the normalized depth as uint8 array.
        """
        depth = self.thisptr.normalizeMeasure(DEPTH, min, max)
        cdef np.ndarray[np.uint8_t, ndim=3] mat = np.empty((depth.height, depth.width, depth.channels), dtype=np.uint8)
        cdef np.uint8_t* mat_buff = <np.uint8_t*> mat.data
        memcpy(mat_buff, depth.data, depth.height*depth.width*depth.channels)

        return np.ascontiguousarray(mat[..., 0])

    def retrieveDepth(self):
        """Retrieve Depth

        Downloads the depth from the device and returns the CPU buffer.
        The retrieve function should be called after the function grab().

        Returns:
            depth (array): the depth as float32 array.
        """

        depth = self.thisptr.retrieveMeasure(DEPTH)
        cdef np.ndarray[np.float32_t, ndim=2] mat = np.empty((depth.height, depth.width), dtype=np.float32)
        cdef np.float32_t* mat_buff = <np.float32_t*> mat.data
        memcpy(mat_buff, depth.data, depth.height*depth.width*4)

        return mat

    def retrieveImage(self, side=LEFT):
        """Retrieve Image

        Downloads the rectified image from the device and returns the CPU buffer.
        The retrieve function should be called after the function grab().

        Args:
            side (optional[zed.SIDE]): defines the image side wanted (zed.LEFT, zed.RIGHT)

        Returns:
            image (array): the requested image as numpy array of type uint8.
        """

        img = self.thisptr.retrieveImage(side)
        cdef np.ndarray[np.uint8_t, ndim=3] mat = np.empty((img.height, img.width, img.channels), dtype=np.uint8)
        cdef np.uint8_t* mat_buff = <np.uint8_t*> mat.data
        memcpy(mat_buff, img.data, img.height*img.width*img.channels)

        return mat

    def getView(self, view_mode=STEREO_SBS):
        """Retrieve View

        Gets a CPU image to display. Several modes available SidebySide, anaglyph...

        Args:
            view_mode (optional[zed.VIEW_MODE]) : the wanted mode (STEREO_LEFT, ...)

        Returns:
            view : the requested view as numpy array of type uint8.
        """

        mat = self.thisptr.getView(view_mode)
        cdef np.ndarray[np.uint8_t, ndim=3] img = np.empty((mat.height, mat.width, mat.channels), dtype=np.uint8)
        cdef np.uint8_t* img_buff = <np.uint8_t*> img.data
        memcpy(img_buff, mat.data, mat.height*mat.width*mat.channels)

        return img

    def getSVONumberOfFrames(self):
        """Get SVO Position

        Get the number of frames in the SVO file.

        Returns:
            the total number of frames in the SVO file (-1 if the SDK is not reading a SVO)
        """

        return self.thisptr.getSVONumberOfFrames()

    def getSVOPosition(self):
        """Get SVO Position

        Get the current position of the SVO file.

        Returns:
            the current position in the SVO file as int (-1 if the SDK is not reading a SVO)
        """

        return self.thisptr.getSVOPosition()

    def setSVOPosition(self, frame):
        """Set SVO Position

        Sets the position of the SVO file to a desired frame.

        Args:
            frame	: the number of the desired frame to be decoded.

        Returns:
            true if success, false if failed (i.e. if the ZED is currently used in live and not playing a SVO file)
        """

        return self.thisptr.setSVOPosition(frame)
