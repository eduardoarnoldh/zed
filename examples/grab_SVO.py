#!/usr/bin/env python
from __future__ import division
import numpy as np
import scipy.io as sio
import cv2
import zed
import sys
import os

def main(SVO_path):

    base_mat_path = os.path.splitext(SVO_path)[0]
    mat_index = 0

    cam = zed.ZEDCamera(SVO_filepath=SVO_path)

    cv2.namedWindow('Depth')
    while not cam.grab(zed.FULL):

        normalized = cam.normalizeDepth()
        depth = cam.retrieveDepth()
        cv2.imshow('Depth', normalized)

        if cv2.waitKey(10) > 0:
            dst_path = '{base}_{ind}.mat'.format(base=base_mat_path, ind=mat_index)
            sio.savemat(dst_path, {'depth':depth}, do_compression=True)
            mat_index += 1

    cv2.destroyAllWindows()


if __name__ == '__main__':
    main(sys.argv[1])
